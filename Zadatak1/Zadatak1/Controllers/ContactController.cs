﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Zadatak1.Models;
using Zadatak1.Services;

namespace Zadatak1.Controllers
{
    public class ContactController : ApiController
    {
        private ContactRepository contactRepository;

        public ContactController()
        {
            this.contactRepository = new ContactRepository();
        } 
        public Contact[] Get()
        {
            return contactRepository.GetAllContacts();
        }
    }
}
